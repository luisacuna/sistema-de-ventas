﻿using _2BusinessLayer;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Automation.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for ProductP.xaml
    /// </summary>
    public partial class ProductP : UserControl
    {
        private DataTable Products;
        private string Answer;

        public ProductP()
        {
            InitializeComponent();
            Products = new DataTable();
        }

        #region Global methods

        //Load all products
        private void ShowProducts()
        {

            (Products, Answer) = ProductB.ShowProducts();

            if (Answer != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar los productos: " + Answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(Products);
            }
        }

        private void SetDataGridContext(DataTable contextTable)
        {
            if (contextTable == null)
            {
                productsDG.DataContext = contextTable;
            }
            else
            {
                productsDG.DataContext = contextTable;
                txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
            }
        }

        private void CheckSearchResults(DataTable results, string answer)
        {
            if (answer != null)
            {
                MessageBox.Show("ERROR! No se han logrado cargar los datos!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(results);
                searchResultsRdb.Visibility = Visibility.Visible;
                searchResultsRdb.IsChecked = true;

                cancelSearchBtn.Visibility = Visibility.Visible;
            }
        }

        private void DataFilter(string productAvailability)
        {
            DataTable filteredProducts;

            try
            {
                if (productAvailability.Equals("AVAILABLE"))
                {
                    filteredProducts = Products.Select("Stock >'" + 0 + "'").CopyToDataTable();

                    SetDataGridContext(filteredProducts);
                }
                else if (productAvailability.Equals("SOLD"))
                {
                    filteredProducts = Products.Select("Stock ='" + 0 + "'").CopyToDataTable();

                    SetDataGridContext(filteredProducts);
                }
            }
            catch
            {
                SetDataGridContext(null);
            }

        }

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            allProductsRdb.IsChecked = true;
        }

        #region CmbSearchSelection change selection

        private void CmbSearchSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string hintCaseId = "Buscar producto por código";
            string hintCaseName = "Buscar producto por nombre";

            switch (((ComboBox)sender).SelectedIndex)
            {
                case 0:
                    // Hide everything required to search products by supplier and category
                    cmbSearchProductBySupplier.Visibility = Visibility.Hidden;
                    cmbSearchProductByCategory.Visibility = Visibility.Hidden;
                    btnSearch.Visibility = Visibility.Hidden;

                    // Make the TextBox visible and set its respective hint
                    txtSeeker.Visibility = Visibility.Visible;
                    txtSeeker.Clear();
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, hintCaseId);

                    break;
                case 1:
                    // Hide everything required to search products by supplier and category
                    cmbSearchProductBySupplier.Visibility = Visibility.Hidden;
                    cmbSearchProductByCategory.Visibility = Visibility.Hidden;
                    btnSearch.Visibility = Visibility.Hidden;

                    // Make the text box visible and set its respective hint
                    txtSeeker.Visibility = Visibility.Visible;
                    txtSeeker.Clear();
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, hintCaseName);

                    break;
                case 2:
                    txtSeeker.Visibility = Visibility.Hidden;
                    cmbSearchProductBySupplier.Visibility = Visibility.Hidden;
                    cmbSearchProductByCategory.Visibility = Visibility.Visible;
                    btnSearch.Visibility = Visibility.Visible;

                    txtSeeker.Clear();

                    SearchCase(cmbSearchProductByCategory);

                    break;
                case 3:
                    txtSeeker.Visibility = Visibility.Hidden;
                    cmbSearchProductByCategory.Visibility = Visibility.Hidden;
                    cmbSearchProductBySupplier.Visibility = Visibility.Visible;
                    btnSearch.Visibility = Visibility.Visible;

                    txtSeeker.Clear();

                    SearchCase(cmbSearchProductBySupplier);

                    break;
            }
        }

        #endregion

        #region Fill ComboBoxes to search product according Product Category or Product Supplier

        private void SearchCase(ComboBox searchCase)
        {
            if(searchCase.Name == "cmbSearchProductByCategory")
            {
                DataTable productsOfCategory;
                string answer;

                (productsOfCategory, answer) = ProductCategoryB.ShowProductCategories();

                if (answer != null)
                {
                    MessageBox.Show("Ha ocurrido un problema al cargar las categorías de productos: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    cmbSearchProductByCategory.DataContext = productsOfCategory;
                    cmbSearchProductByCategory.SelectedValuePath = "Código categoría";
                    cmbSearchProductByCategory.DisplayMemberPath = "Nombre";
                }
                
            }
            else if (searchCase.Name == "cmbSearchProductBySupplier")
            {
                DataTable productsSupplier;
                string answer;

                (productsSupplier, answer) = SupplierB.ShowSuppliers();

                if (answer != null)
                {
                    MessageBox.Show("Ha ocurrido un problema al cargar los proveedores de productos: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    cmbSearchProductBySupplier.DataContext = productsSupplier;
                    cmbSearchProductBySupplier.SelectedValuePath = "Código proveedor";
                    cmbSearchProductBySupplier.DisplayMemberPath = "Nombre del proveedor";
                }
            }
        }

        #endregion

        #region Automatically search for products with text in TextBox txtSeeker by Id or Name    

        private void TxtSeeker_KeyDown(object sender, KeyEventArgs e)
        {
            //Validate not entering letters in txtSeeker when index "Código" is selected in cmbSearchSelection
            if (cmbSearchSelection.SelectedIndex == 0)
            {
                if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void TxtSeeker_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(txtSeeker.Text.Length == 0)
            {
                ShowProducts();
            }
            else
            {
                DataTable dataTable;
                string answer;

                switch (cmbSearchSelection.SelectedIndex)
                {
                    case 0:
                        (dataTable, answer) = ProductB.ShowProductById(Convert.ToInt32(txtSeeker.Text));

                        CheckSearchResults(dataTable, answer);

                        //SearchProductByIdOrName(searchModeCaseId, txtSeeker.Text);

                        break;
                    case 1:

                        (dataTable, answer) = ProductB.ShowProductByName(txtSeeker.Text);

                        CheckSearchResults(dataTable, answer);

                        //SearchProductByIdOrName(searchModeCaseName, txtSeeker.Text);
                        break;
                }
            }

        }

        #endregion

        #region BtnSearch Click to search products by Id Category Or Id Supplier

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            DataTable products;
            string answer;

            try
            {
                if (cmbSearchProductByCategory.IsVisible)
                {
                    (products, answer) = ProductB.ShowProductsByCategoryId(Int32.Parse(cmbSearchProductByCategory.SelectedValue.ToString()));

                    CheckSearchResults(products, answer);

                }
                if (cmbSearchProductBySupplier.IsVisible)
                {
                    (products, answer) = ProductB.ShowProductBySupplierId(Int32.Parse(cmbSearchProductBySupplier.SelectedValue.ToString()));

                    CheckSearchResults(products, answer);
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("ERROR! Debe seleccionar un índice de búsqueda\n\n" + ex, "MESAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region All, Available, Unavailable products and SearchResult RadioButtons

        private void AllProductsRdb_Checked(object sender, RoutedEventArgs e)
        {
            if (cancelSearchBtn.IsVisible)
            {
                SetDataGridContext(Products);
            }
            else
            {
                MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, "Seleccione un método de búsqueda");
                ShowProducts();
            }
        }

        private void AvaProductsRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("AVAILABLE");
        }

        private void UnavaProductsRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("SOLD");
        }

        private void SearchResultsRdb_Checked(object sender, RoutedEventArgs e)
        {
            allProductsRdb.IsEnabled = false;
            avaProductsRdb.IsEnabled = false;
            unavaProductsRdb.IsEnabled = false;
        }

        #endregion

        //Cancel search button
        private void CancelSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            allProductsRdb.IsChecked = true;

            searchResultsRdb.Visibility = Visibility.Hidden;

            cancelSearchBtn.Visibility = Visibility.Hidden;

            allProductsRdb.IsEnabled = true;
            avaProductsRdb.IsEnabled = true;
            unavaProductsRdb.IsEnabled = true;
            txtSeeker.Clear();
        }

        #region Buttons new & update

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {
            ProductPFrm ppf = new ProductPFrm();
            bool? dialogResult = ppf.ShowDialog();

            if (dialogResult == true)
            {
                ShowProducts();
                allProductsRdb.IsChecked = true;
            }

        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView drv = productsDG.SelectedItem as DataRowView;
                int id = Convert.ToInt32(drv[0]);

                ProductPFrm ppf = new ProductPFrm { ProductId = id };
                bool? dialogResult = ppf.ShowDialog();

                if (dialogResult == true)
                {
                    ShowProducts(); 
                    allProductsRdb.IsChecked = true;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("ERROR! Debe seleccionar una fila para poder editar", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        #region Extend & Collapse AdditionalButtonsGrid

        private void CollapseAdditionalBGridBtn_Click(object sender, RoutedEventArgs e)
        {
            collapseAdditionalBGridBtn.Visibility = Visibility.Hidden;
            extendAdditionalBGridBtn.Visibility = Visibility.Visible;            
        }

        // At the end of collapsing the additionalButtonGrid
        private void DoubleAnimationUsingKeyFrames_Completed(object sender, EventArgs e)
        {
            additionalButtonsGrid.Background = productsDG.Background;
        }

        private void ExtendAdditionalBGridBtn_Click(object sender, RoutedEventArgs e)
        {
            extendAdditionalBGridBtn.Visibility = Visibility.Hidden;
            collapseAdditionalBGridBtn.Visibility = Visibility.Visible;

            additionalButtonsGrid.Background = System.Windows.Media.Brushes.White;
        }

        #endregion

        private void ProductCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            ProductCategoryFrm pcf = new ProductCategoryFrm();
            pcf.ShowDialog();
        }

        private void ProductSaleBtn_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
