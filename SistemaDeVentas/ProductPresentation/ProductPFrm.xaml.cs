﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using _2BusinessLayer;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for ProductPFrm.xaml
    /// </summary>
    public partial class ProductPFrm : Window
    {
        public int ProductId { get; set; }

        public ProductPFrm()
        {
            InitializeComponent();
        }

        #region Load event

        //IT SHOWS THE RESPECTIVE INDEX OF SUPPLIERCMB AND CATEGORYCMB OF THE PRODUCT TO EDIT
        private int CmbIndexOf(string text, string caseText)
        {
            int index = 0;
            DataTable dataTable;

            if(caseText == "Supplier")
            {
                dataTable = supplierCmb.DataContext as DataTable;

                foreach (DataRow dr in dataTable.Rows)
                {
                    if (dr[2].ToString().Equals(text))
                    {
                        index = Convert.ToInt32(dr[0]);
                        break;
                    }
                }
            }
            else if(caseText == "Category")
            {
                dataTable = categoryCmb.DataContext as DataTable;

                foreach (DataRow dr in dataTable.Rows)
                {
                    if (dr[1].ToString().Equals(text))
                    {
                        index = Convert.ToInt32(dr[0]);
                        break;
                    }
                }
            }

            return index;
        }

        //LOAD DATA OF SUPPLIERSCMB & CATEGORYCMB
        private void LoadDataOfComboBoxes()
        {
            DataTable productCategories;
            string answerCategory;

            DataTable productSuppliers;
            string answerSupplier;

            (productCategories, answerCategory) = ProductCategoryB.ShowProductCategories();

            (productSuppliers, answerSupplier) = SupplierB.ShowSuppliers();

            if (answerCategory != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar las categorías de productos: " + answerCategory, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            else if (answerSupplier != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar los proveedores de productos: " + answerSupplier, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                categoryCmb.DataContext = productCategories;
                categoryCmb.SelectedValuePath = "Código categoría";
                categoryCmb.DisplayMemberPath = "Nombre";

                supplierCmb.DataContext = productSuppliers;
                supplierCmb.SelectedValuePath = "Código proveedor";
                supplierCmb.DisplayMemberPath = "Nombre del proveedor";
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDataOfComboBoxes();

            if (ProductId != 0)
            {
                DataRow dr;
                string answer;

                (dr, answer) = ProductB.ProductByIdAsDataRow(ProductId);

                if (answer != null)
                {
                    MessageBox.Show("Ha ocurrido un problema al cargar los datos del producto: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    //Set the product supplier to supplierCmb
                    string supplier = dr[1].ToString();
                    int indexOfSupplier = CmbIndexOf(supplier, "Supplier");
                    supplierCmb.SelectedValue = indexOfSupplier;

                    //Set the product name to nameTxt
                    nameTxt.Text = dr[2].ToString();

                    //Set the product description to descriptionTxt
                    descriptionTxt.Text = dr[3].ToString();

                    //Set the product category to categoryCmb
                    string category = dr[5].ToString();
                    int indexOfCategory = CmbIndexOf(category, "Category");
                    categoryCmb.SelectedValue = indexOfCategory;
                }

            }
        }

        #endregion

        #region Agree, Cancel & Close buttons

        private void AgreeBtn_Click(object sender, RoutedEventArgs e)
        {
            supplierTxbk.Foreground = System.Windows.Media.Brushes.Black;
            nameTxbk.Foreground = System.Windows.Media.Brushes.Black;
            nameTxbk.Foreground = System.Windows.Media.Brushes.Black;
            categoryTxbk.Foreground = System.Windows.Media.Brushes.Black;

            if (supplierCmb.SelectedItem == null)
            {
                MessageBox.Show("ERROR! Debe seleccionar el proveedor del producto!\n\nDescripción:\nNo se ha seleccionado el proveedor del artículo\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo producto al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                
                supplierTxbk.Foreground = System.Windows.Media.Brushes.Red;

            }
            else if (nameTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir el nombre del producto!\n\nDescripción:\nNo se ha introducido el nombre del artículo\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo producto al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                nameTxbk.Foreground = System.Windows.Media.Brushes.Red;

            }
            else if (descriptionTxt.Text.Length.Equals(0))
            {
                MessageBox.Show("ERROR! Debe introducir la descripción del producto!\n\nDescripción:\nNo se ha introducido la descripción del artículo\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo producto al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                nameTxbk.Foreground = System.Windows.Media.Brushes.Red;

            }
            else if (categoryCmb.SelectedItem == null)
            {
                MessageBox.Show("ERROR! Debe seleccionar la categoría del producto!\n\nDescripción:\nNo se ha seleccionado la categoría a la que pertenece el artículo\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo producto al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                categoryTxbk.Foreground = System.Windows.Media.Brushes.Red;

            }
            else
            {
                if (ProductId != 0)
                {
                    int idSupplier = Int32.Parse(supplierCmb.SelectedValue.ToString());
                    string productName = nameTxt.Text;
                    string productDescription = descriptionTxt.Text;

                    int idCategory = Int32.Parse(categoryCmb.SelectedValue.ToString());

                    string answer = ProductB.UpdateProduct(ProductId, idSupplier, productName, productDescription, idCategory);

                    if (answer == "DONE")
                    {
                        MessageBox.Show("Se ha editado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.DialogResult = true;

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("ERROR! No se editó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {

                    MessageBoxResult result = MessageBox.Show("¿Está seguro que desea guardar los datos del producto?", "MENSAJE DEL SISTEMA", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if(result == MessageBoxResult.Yes)
                    {
                        int idSupplier = Int32.Parse(supplierCmb.SelectedValue.ToString());
                        string productName = nameTxt.Text;
                        string productDescription = descriptionTxt.Text;

                        int idCategory = Int32.Parse(categoryCmb.SelectedValue.ToString());

                        string answer = ProductB.InsertProduct(idSupplier, productName, productDescription, idCategory);

                        if(answer == "DONE")
                        {
                            MessageBox.Show("Se ha ingresado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                            this.DialogResult = true;

                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }                   

                }
            }

        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            CloseBtn_Click(sender, e);
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        #endregion

    }
}
