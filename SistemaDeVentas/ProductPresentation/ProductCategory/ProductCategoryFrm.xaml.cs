﻿using _2BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for ProductCategoryFrm.xaml
    /// </summary>
    public partial class ProductCategoryFrm : Window
    {
        public int CategoryId { get; set; }

        private readonly Storyboard ShowCategoryEditor;
        private readonly Storyboard HideCategoryEditor;

        public ProductCategoryFrm()
        {
            InitializeComponent();


            //Extend updateCategoryGrid

            DoubleAnimation collapseDataGrid = new DoubleAnimation { From = 400, To = 0, Duration = new Duration(TimeSpan.FromSeconds(0.3)), 
                AutoReverse = false };

            DoubleAnimation extendUpdateCatGrid = new DoubleAnimation { From = 0, To = 400, Duration = new Duration(TimeSpan.FromSeconds(0.3)),
                AutoReverse = false };

            ShowCategoryEditor = new Storyboard();
            ShowCategoryEditor.Children.Add(collapseDataGrid);
            ShowCategoryEditor.Children.Add(extendUpdateCatGrid);

            Storyboard.SetTargetName(collapseDataGrid, productCategoryDG.Name);
            Storyboard.SetTargetProperty(collapseDataGrid, new PropertyPath(DataGrid.WidthProperty));

            Storyboard.SetTargetName(extendUpdateCatGrid, updateCatGrid.Name);
            Storyboard.SetTargetProperty(extendUpdateCatGrid, new PropertyPath(Grid.WidthProperty));



            //Collapse updateCategoryGrid

            DoubleAnimation extendDataGrid = new DoubleAnimation { From = 0, To = 400, Duration = new Duration(TimeSpan.FromSeconds(0.3)),
                AutoReverse = false };

            DoubleAnimation collapseUpdateCatGrid = new DoubleAnimation { From = 400, To = 0, Duration = new Duration(TimeSpan.FromSeconds(0.3)),
                AutoReverse = false };

            HideCategoryEditor = new Storyboard();
            HideCategoryEditor.Children.Add(extendDataGrid);
            HideCategoryEditor.Children.Add(collapseUpdateCatGrid);

            Storyboard.SetTargetName(extendDataGrid, productCategoryDG.Name);
            Storyboard.SetTargetProperty(extendDataGrid, new PropertyPath(DataGrid.WidthProperty));

            Storyboard.SetTargetName(collapseUpdateCatGrid, updateCatGrid.Name);
            Storyboard.SetTargetProperty(collapseUpdateCatGrid, new PropertyPath(Grid.WidthProperty));

        }

        #region Global methods

        //Load all product categories
        private void ShowCategories()
        {
            DataTable categories;
            string answer;

            (categories, answer) = ProductCategoryB.ShowProductCategories();

            if (answer != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar los productos: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(categories);
            }
        }

        private void SetDataGridContext(DataTable contextTable)
        {
            productCategoryDG.DataContext = contextTable;
            txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
        }

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShowCategories();
        }

        #region New & Update Buttons (Available in productCategoryDG)

        // Modify the interface to allow the user to add or edit a product category
        private void AddOrUpdate(bool addOrUpdate, string mode) // Add Or Update ; Cancel only to fill parameter
        {
            if (addOrUpdate)
            {
                //Hide header grid
                headerGrid.Visibility = Visibility.Hidden;

                // Set background of showCategoryItem in transparent and disable showCategoryItem
                showCategoryItem.Background = Brushes.Transparent;
                showCategoryItem.IsEnabled = false;

                // Set the background of productCategoryDG as the background of insertOrUpdateCategoryItem and enable insertOrUpdateCategoryItem
                insertOrUpdateCategoryItem.Background = productCategoryDG.Background;
                insertOrUpdateCategoryItem.IsEnabled = true;

                // Hide productCategoryDGButtons (available Buttons when showing product categories)
                productCategoryDGButtons.Visibility = Visibility.Hidden;

                // Hide TextBlocks "Registros" and count of records showed in the DataGrid
                txbkRecords.Visibility = Visibility.Hidden;
                txbkRecordsCount.Visibility = Visibility.Hidden;

                // Show updateCatGridButtons (available Buttons when adding or editing a product category)
                updateCatGridButtons.Visibility = Visibility.Visible;

                if (mode.Equals("Add"))
                {
                    // Hide the count of products of the category in edition
                    availableWhenEditingSkpn.Visibility = Visibility.Hidden; //Contains productCountOfCategoryTxbk & productCountOfCategoryTxt
                }
                else if(mode.Equals("Update"))
                {
                    // Set "Edición:" as content of insertOrUpdate CategoryItem
                    insertOrUpdateCategoryItem.Content = "Edición:";

                    availableWhenEditingSkpn.Visibility = Visibility.Visible; //Contains productCountOfCategoryTxbk & productCountOfCategoryTxt;
                }
            }
            else
            {
                //Show header grid
                headerGrid.Visibility = Visibility.Visible;

                // Set the background of productCategoryDG as the background of showCategoryItem and enable showCategoryItem
                showCategoryItem.Background = productCategoryDG.Background;
                showCategoryItem.IsEnabled = true;

                // Set background of insertOrUpdateCategoryItem in transparent and disable insertOrUpdateCategoryItem
                insertOrUpdateCategoryItem.Background = Brushes.Transparent;
                insertOrUpdateCategoryItem.IsEnabled = false;

                // Show productCategoryDGButtons (available Buttons when showing product categories)
                productCategoryDGButtons.Visibility = Visibility.Visible;

                // Show TextBlocks "Registros" and count of records showed in the DataGrid
                txbkRecords.Visibility = Visibility.Visible;
                txbkRecordsCount.Visibility = Visibility.Visible;

                // Hide updateCatGridButtons (available Buttons when adding or editing a product category)
                updateCatGridButtons.Visibility = Visibility.Hidden;

                // Set "Nuevo:" as content of insertOrUpdate CategoryItem
                insertOrUpdateCategoryItem.Content = "Nuevo:";

                // Hide the count of products of the category in edition
                availableWhenEditingSkpn.Visibility = Visibility.Hidden; //Contains productCountOfCategoryTxbk & productCountOfCategoryTxt
            }
        }

        private void newBtn_Click(object sender, RoutedEventArgs e)
        {
            ShowCategoryEditor.Begin(this);

            AddOrUpdate(true, "Add");
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView drv = productCategoryDG.SelectedItem as DataRowView;
                CategoryId = Convert.ToInt32(drv[0]);

                ShowCategoryEditor.Begin(this);

                AddOrUpdate(true, "Update");

                // Load category data to edit
                LoadDataToEdit(CategoryId);

            }
            catch (NullReferenceException)
            {
                MessageBox.Show("ERROR! Debe seleccionar una fila para poder editar", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LoadDataToEdit(int idCategory)
        {
            DataRow dr;
            string answer;

            int productCount;

            (dr, answer) = ProductCategoryB.ProductCategoryByIdAsDataRow(idCategory);

            if (answer != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar los datos de la categoría: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                productCategoryNameTxt.Text = dr[1].ToString();

                productCount = ProductCategoryB.ProductCountOfACategory(CategoryId);

                productCountOfCategoryTxt.Text = productCount.ToString();

            }
        }

        #endregion

        #region Agree & Cancel Buttons (Available in updateCatGrid)

        private void CleanComponents()
        {
            productCategoryNameTxt.Clear();
            productCountOfCategoryTxt.Clear();
        }

        private void agreeBtn_Click(object sender, RoutedEventArgs e)
        {
            string answer;

            productCategoryNameTxbk.Foreground = System.Windows.Media.Brushes.Black;

            if (productCategoryNameTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir el nombre de la categoría!\n\nDescripción:\nNo se ha introducido el nombre de la categoría de productos\n, " +
                    "mientras no lo haga, no puede agregarse la categoría al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                productCategoryNameTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else
            {
                if (CategoryId != 0)
                {
                    string categoryName = productCategoryNameTxt.Text;

                    answer = ProductCategoryB.UpdateProductCategory(CategoryId, categoryName);

                    if(answer == "DONE")
                    {
                        MessageBox.Show("Se ha editado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                        CleanComponents();

                        // Collapse updateCatGrid and all elements related to it & reset CategoryId value
                        cancelBtn_Click(sender, e);

                        // Refresh productCategoryDG
                        ShowCategories();
                    }
                    else
                    {
                        MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    string categoryName = productCategoryNameTxt.Text;

                    answer = ProductCategoryB.InsertProductCategory(categoryName);

                    if (answer == "DONE")
                    {
                        MessageBox.Show("Se ha ingresado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                        CleanComponents();

                        // Collapse updateCatGrid and all elements related to it
                        cancelBtn_Click(sender, e);

                        // Refresh productCategoryDG
                        ShowCategories();
                    }
                    else
                    {
                        MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            HideCategoryEditor.Begin(this);

            // Reset CategoryId value
            CategoryId = 0;

            AddOrUpdate(false, "Cancel");
        }

        #endregion

        // Close window
        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #region searchBtn & cancelSearch Click to search product category by Category Id

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            DataTable dataTable;
            string answer;

            (dataTable, answer) = ProductCategoryB.ShowProductCategoryById(Int32.Parse(seekerTxt.Text));

            if(answer != null)
            {
                MessageBox.Show("ERROR! No se ha logrado cargar la categoría!\n Descripción:\n\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(dataTable);

                searchBtn.Visibility = Visibility.Hidden;
                cancelSearchBtn.Visibility = Visibility.Visible;
            }
        }

        private void cancelSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            searchBtn.Visibility = Visibility.Visible;
            cancelSearchBtn.Visibility = Visibility.Hidden;

            ShowCategories();
            seekerTxt.Clear();
        }

        #endregion

    }
}
