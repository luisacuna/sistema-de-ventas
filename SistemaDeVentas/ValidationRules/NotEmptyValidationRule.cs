﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;
using System.Windows.Resources;

namespace SistemaDeVentas
{
    public class NotEmptyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (((string)value).Equals(string.Empty))
                return new ValidationResult(false, "Campo requerido");
            else
                return ValidationResult.ValidResult;
        }
    }
}
