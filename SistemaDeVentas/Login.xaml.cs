﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using _2BusinessLayer;
using MaterialDesignColors.ColorManipulation;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public int IdEmployee { get; set; }

        public Login()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            usernameTxt.Focus();
        }

        #region Show & Hide password

        private void YouCantSeePassword_Click(object sender, RoutedEventArgs e)
        {
            passwordPswb.Visibility = Visibility.Visible;
            passwordTxt.Visibility = Visibility.Hidden;

            //passwordPswb.Password = passwordTxt.Text;

            youCantSeePassword.Visibility = Visibility.Hidden;
            youCanSeePassword.Visibility = Visibility.Visible;
        }

        private void YouCanSeePassword_Click(object sender, RoutedEventArgs e)
        {
            passwordPswb.Visibility = Visibility.Hidden;
            passwordTxt.Visibility = Visibility.Visible;

            //passwordTxt.Text = passwordPswb.Password;

            youCantSeePassword.Visibility = Visibility.Visible;
            youCanSeePassword.Visibility = Visibility.Hidden;
        }

        #endregion

        #region SignIn & Close buttons

        private void SingInBtn_Click(object sender, RoutedEventArgs e)
        {
            MaterialDesignThemes.Wpf.HintAssist.SetForeground(usernameTxt, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));
            MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(usernameTxt, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));

            MaterialDesignThemes.Wpf.HintAssist.SetForeground(passwordPswb, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));
            MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(passwordPswb, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));

            MaterialDesignThemes.Wpf.HintAssist.SetForeground(passwordTxt, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));
            MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(passwordTxt, (Brush)(new BrushConverter().ConvertFrom("#03a9f4")));

            if (usernameTxt.Text.Equals(string.Empty))
            {
                MessageBox.Show("Nombre de usuario requerido!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                MaterialDesignThemes.Wpf.HintAssist.SetForeground(usernameTxt, Brushes.Red);
                MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(usernameTxt, Brushes.Red);
                usernameTxt.Focus();
            }
            else if (passwordPswb.Password.Equals(string.Empty) || passwordTxt.Text.Equals(string.Empty))
            {
                MessageBox.Show("Contraseña requerida!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                if (passwordPswb.Visibility == Visibility.Visible)
                {
                    MaterialDesignThemes.Wpf.HintAssist.SetForeground(passwordPswb, Brushes.Red);
                    MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(passwordPswb, Brushes.Red);
                    passwordPswb.Focus();
                }
                else if (passwordTxt.Visibility == Visibility.Visible)
                {
                    MaterialDesignThemes.Wpf.HintAssist.SetForeground(passwordTxt, Brushes.Red);
                    MaterialDesignThemes.Wpf.TextFieldAssist.SetUnderlineBrush(passwordTxt, Brushes.Red);
                    passwordPswb.Focus();
                }
            }
            else
            {
                string answer;

                (IdEmployee, answer) = LoginB.AppLogin_Verifier(usernameTxt.Text, passwordPswb.Password);

                if (answer != null)
                {
                    MessageBox.Show(answer, "MESAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (IdEmployee > 0)
                {
                    //LoadingWindow lw = new LoadingWindow() /*{ Owner = this }*/;
                    //lw.ShowDialog();
                    MainWindow mw = new MainWindow { IdEmployee = IdEmployee };

                    Thread t = new Thread(/*ShowLoadingWindow*/() =>
                    {
                        new LoadingWindow().Show();
                    });
                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();

                    mw.Show();

                    do
                    {
                        if (mw.IsLoaded)
                        {
                            //if (t.ThreadState == System.Threading.ThreadState.Running)
                            //{
                            t.Interrupt();
                            this.Close();

                            //}
                        }
                    } while (!mw.IsLoaded);
                }
                else
                {
                    MessageBox.Show("Credenciales erróneas", "MESAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion
        
        #region UsernameTxt, PasswordPswb & PasswordTxt KeyUp event

        private void UsernameTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                SingInBtn_Click(sender, e);
            }
        }

        private void PasswordPswb_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                SingInBtn_Click(sender, e);
            }
        }

        private void PasswordTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                SingInBtn_Click(sender, e);
            }
        }

        #endregion

        #region PasswordPswb PasswordChanged & PasswordTxt TextChanged

        private void PasswordPswb_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (passwordPswb.IsVisible)
            {
                passwordTxt.Text = passwordPswb.Password; 
            }
        }

        private void PasswordTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (passwordTxt.IsVisible)
            {
                passwordPswb.Password = passwordTxt.Text; 
            }
        }

        #endregion

    }
}
