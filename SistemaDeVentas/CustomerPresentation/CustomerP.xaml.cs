﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using _2BusinessLayer;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for CustomerP.xaml
    /// </summary>
    public partial class CustomerP : UserControl
    {
        private DataTable Customers;
        private string Answer;

        public CustomerP()
        {
            InitializeComponent();
            Customers = new DataTable();
        }

        #region Global methods

        /// <summary>
        /// LOAD ALL CUSTOMERS
        /// </summary>
        private void ShowCustomers()
        {
            (Customers, Answer) = CustomerB.ShowCustomers();

            if (Answer != null)
            {
                MessageBox.Show("Ha ocurrido un problema al cargar los clientes\n\nDescripción:\n" + Answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(Customers);
            }
        }

        /// <summary>
        /// SET THE DATACONTEXT TO DATAGRID.
        /// A CONTEXT TABLES IS REQUIRED TO SET THE DATACONTEXT
        /// </summary>
        /// <param name="contextTable"></param>
        private void SetDataGridContext(DataTable contextTable)
        {
            if(contextTable == null)
            {
                customersDG.DataContext = contextTable;
            }
            else
            {
                customersDG.DataContext = contextTable;
                txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
            }
        }

        private void DataFilter(string customerStatus)
        {
            DataTable filteredCustomers;

            try
            {
                filteredCustomers = Customers.Select("Estado = '" + customerStatus + "'").CopyToDataTable();

                SetDataGridContext(filteredCustomers);
            }
            catch
            {
                SetDataGridContext(null);
            }
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            allCustomersRdb.IsChecked = true;
        }

        #region CmbSearchSelection change selection

        private void CmbSearchSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string caseId = "Buscar cliente por código";
            string caseIdentificationCard = "Buscar cliente por No. de cédula";
            string casePhoneNumber = "Buscar cliente por No. de teléfono";
            string caseNameOrLastname = "Buscar cliente por nombre o apellido";
            string caseBillingAddress = "Buscar cliente por dirección de envío";
            string caseCity = "Buscar cliente por ciudad";

            switch (((ComboBox)sender).SelectedIndex)
            {
                case 0:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, caseId);
                    break;
                case 1:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, caseIdentificationCard);
                    break;
                case 2:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, casePhoneNumber);
                    break;
                case 3:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, caseNameOrLastname);
                    break;
                case 4:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, caseCity);
                    break;
                case 5:
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, caseBillingAddress);
                    break;
            }
        }

        #endregion

        #region RedioButtons AllCustomers, ActiveCustomers & InactiveCustomers

        private void AllCustomersRdb_Checked_1(object sender, RoutedEventArgs e)
        {
            // OVERRIDE ALL OTHER SEARCH METHODS
            cmbSearchSelection.SelectedItem = null;

            MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, "Seleccione un método de búsqueda");
            ShowCustomers();
        }

        private void ActiveCustomersRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("ACTIVO");
        }

        private void InactiveCustomersRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("INACTIVO");
        }

        #endregion

        #region Automatically search for Customer with text in TextBox txtSeeker

        private void TxtSeeker_KeyDown(object sender, KeyEventArgs e)
        {
            //Validate not entering letters in txtSeeker when index "Código" is selected in cmbSearchSelection
            if ((cmbSearchSelection.SelectedIndex.Equals(0)) || (cmbSearchSelection.SelectedIndex.Equals(2)))
            {
                if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void TxtSeeker_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSeeker.Text.Length == 0)
            {
                ShowCustomers();
            }
            else
            {
                DataTable customers;
                string answer;

                switch (cmbSearchSelection.SelectedIndex)
                {
                    // CUSTOMER ID
                    case 0:
                        int mode = 0; //It is the way used to display customer data by id

                        (customers, answer) = CustomerB.ShowCustomerById(Convert.ToInt32(txtSeeker.Text), mode);

                        CheckSearchResults(customers, answer);

                        break;
                        
                    // CUSTOMER IDENTIFICATION CARD
                    case 1:
                        (customers, answer) = CustomerB.ShowCustomerByIdentificationCard(txtSeeker.Text);

                        CheckSearchResults(customers, answer);

                        break;

                    // CUSTOMER PHONE NUMBER
                    case 2:
                        (customers, answer) = CustomerB.ShowCustomerByPhoneNumber(txtSeeker.Text);

                        CheckSearchResults(customers, answer);

                        break;

                    // CUSTOMER BY NAME OR LASTNAME
                    case 3:
                        (customers, answer) = CustomerB.ShowCustomersByNameOrLastname(txtSeeker.Text);

                        CheckSearchResults(customers, answer);

                        break;

                    // CUSTOMER BY CITY
                    case 4:
                        (customers, answer) = CustomerB.ShowCustomersByCity(txtSeeker.Text);

                        CheckSearchResults(customers, answer);

                        break;

                   //CUSTOMER BY BILLING ADDRESS
                    case 5:
                        (customers, answer) = CustomerB.ShowCustomersByBillingAddres(txtSeeker.Text);

                        CheckSearchResults(customers, answer);

                        break;
                } 
            }
        }

        private void CheckSearchResults(DataTable results, string answer)
        {
            if(answer != null)
            {
                MessageBox.Show("ERROR! No se han logrado cargar los datos!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(results);
                searchResultsRdb.Visibility = Visibility.Visible;
                searchResultsRdb.IsChecked = true;

                cancelSearchBtn.Visibility = Visibility.Visible;
            }
        }

        #endregion

        // CANCEL SEARCH BUTTON
        private void CancelSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            allCustomersRdb.IsChecked = true;

            searchResultsRdb.Visibility = Visibility.Hidden;

            cancelSearchBtn.Visibility = Visibility.Hidden;

            if (txtSeeker.Text.Length != 0)
            {
                txtSeeker.Clear();
            }
        }

        #region New & Update Buttons

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {
            CustomerPFrm cpf = new CustomerPFrm();
            bool? dialogResult = cpf.ShowDialog();

            if (dialogResult == true)
            {
                ShowCustomers();
                allCustomersRdb.IsChecked = true;
            }
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView drv = customersDG.SelectedItem as DataRowView;
                int id = Convert.ToInt32(drv[0]);

                CustomerPFrm cpf = new CustomerPFrm { CustomerId = id };
                bool? dialogResult = cpf.ShowDialog();

                if (dialogResult == true)
                {
                    ShowCustomers();
                    allCustomersRdb.IsChecked = true;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("ERROR! Debe seleccionar una fila para poder editar", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

    }
}