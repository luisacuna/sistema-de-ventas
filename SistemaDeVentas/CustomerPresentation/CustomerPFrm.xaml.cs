﻿using _2BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for CustomerPFrm.xaml
    /// </summary>
    public partial class CustomerPFrm : Window
    {
        public int CustomerId { get; set; }

        public CustomerPFrm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (CustomerId != 0)
            {
                DataRow dr;
                string answer;
                int showMode = 1; //It is the way used to display customer data by id


                (dr, answer) = CustomerB.CustomerByIdAsDataRow(CustomerId, showMode);

                if (answer != null)
                {
                    MessageBox.Show("Ha ocurrido un problema al cargar los datos del cliente: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    identificationCardMsktxt.Text = dr[1].ToString();
                    nameTxt.Text = dr[2].ToString();
                    lastnameTxt.Text = dr[3].ToString();
                    phoneNumberMsktxt.Text = dr[4].ToString();
                    billingAddressTxt.Text = dr[5].ToString();
                    cityTxt.Text = dr[6].ToString();
                    departmentTxt.Text = dr[7].ToString();
                }

            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #region Agree & Cancel buttons

        private void AgreeBtn_Click(object sender, RoutedEventArgs e)
        {
            identificationCardTxbk.Foreground = System.Windows.Media.Brushes.Black;
            nameTxbk.Foreground = System.Windows.Media.Brushes.Black;
            lastnameTxbk.Foreground = System.Windows.Media.Brushes.Black;
            phoneNumberTxbk.Foreground = System.Windows.Media.Brushes.Black;
            cityTxbk.Foreground = System.Windows.Media.Brushes.Black;
            departmentTxbk.Foreground = System.Windows.Media.Brushes.Black;
            ////billingAddressTxbk.Foreground = System.Windows.Media.Brushes.Black;

            if (!identificationCardMsktxt.IsMaskCompleted)
            {
                MessageBox.Show("ERROR! Debe introducir el número de cédula del cliente!\n\nDescripción:\nNo se ha introducido el número de cédula del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                identificationCardTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (nameTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir el nombre del cliente!\n\nDescripción:\nNo se ha introducido el nombre del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                nameTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (lastnameTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir el apellido del cliente!\n\nDescripción:\nNo se ha introducido el apellido del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                lastnameTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (!phoneNumberMsktxt.IsMaskCompleted)
            {
                MessageBox.Show("ERROR! Debe introducir el No. de teléfono del cliente!\n\nDescripción:\nNo se ha introducido No. de teléfono del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                phoneNumberTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (cityTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir la ciudad del cliente!\n\nDescripción:\nNo se ha introducido la ciudad del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                cityTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (departmentTxt.Text.Equals(""))
            {
                MessageBox.Show("ERROR! Debe introducir el departamento del cliente!\n\nDescripción:\nNo se ha introducido el departamento del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                departmentTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else if (billingAddressTxt.Text.Length.Equals(0))
            {
                MessageBox.Show("ERROR! Debe introducir la dirección de envío del cliente!\n\nDescripción:\nNo se ha introducido la dirección de envío del cliente\n, " +
                    "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);

                billingAddressTxbk.Foreground = System.Windows.Media.Brushes.Red;
            }
            else
            {
                if (CustomerId != 0)
                {
                    string answer;

                    string identification = identificationCardMsktxt.Text;
                    string name = nameTxt.Text;
                    string lastname = lastnameTxt.Text;
                    string phoneNumber = phoneNumberMsktxt.Text;
                    string city = cityTxt.Text;
                    string department = departmentTxt.Text;
                    string billingAddress = billingAddressTxt.Text;

                    answer = CustomerB.UpdateCustomer(CustomerId, identification, name, lastname, phoneNumber, billingAddress, city, department);

                    if (answer == "DONE")
                    {
                        MessageBox.Show("Se ha editado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.DialogResult = true;

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("ERROR! No se editó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    string answer;

                    string identification = identificationCardMsktxt.Text;
                    string name = nameTxt.Text;
                    string lastname = lastnameTxt.Text;
                    string phoneNumber = phoneNumberMsktxt.Text;
                    string city = cityTxt.Text;
                    string department = departmentTxt.Text;
                    string billingAddress = billingAddressTxt.Text;

                    answer = CustomerB.InsertCustomer(identification, name, lastname, phoneNumber, billingAddress, city, department);

                    if (answer == "DONE")
                    {
                        MessageBox.Show("Se ha ingresado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.DialogResult = true;

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
            }

        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e) => CloseBtn_Click(sender, e);

        #endregion

    }
}
