﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using _2BusinessLayer;
using System.Windows.Markup;
using System.Threading;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int IdEmployee { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainTitleGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataRow dr;

            dr = EmployeeB.ShowEmployeeByIdAsDataRow(IdEmployee);

            if (!dr[5].ToString().Equals("ADMINISTRACIÓN"))
            {
                itmProducts.Visibility = Visibility.Collapsed;
                itmEmployees.Visibility = Visibility.Collapsed;
                itmSuppliers.Visibility = Visibility.Collapsed;
                itmPurchases.Visibility = Visibility.Collapsed;
            }

        }

        #region Logout & minimize window buttons

        private void LogoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void BtnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        #endregion

        #region Extend and Collapse Menu

        //Show TextBlocks and btnCollapseMenu when extending the menu
        private void BtnExtendMenu_Click(object sender, RoutedEventArgs e)
        {
            btnExtendMenu.Visibility = Visibility.Hidden;
            btnCollapseMenu.Visibility = Visibility.Visible;

            txbkHome.Visibility = Visibility.Visible;
            txbkProducts.Visibility = Visibility.Visible;
            txbkEmployees.Visibility = Visibility.Visible;
            txbkSuppliers.Visibility = Visibility.Visible;
            txbkCustomer.Visibility = Visibility.Visible; 
            txbkPurchases.Visibility = Visibility.Visible;
            txbkSales.Visibility = Visibility.Visible;
        }

        //Hide TextBlocks and show btnExtendMenu when collapsing the menu
        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnExtendMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Hidden;

            txbkHome.Visibility = Visibility.Collapsed;
            txbkProducts.Visibility = Visibility.Collapsed;
            txbkEmployees.Visibility = Visibility.Collapsed;
            txbkSuppliers.Visibility = Visibility.Collapsed;
            txbkCustomer.Visibility = Visibility.Collapsed;
            txbkPurchases.Visibility = Visibility.Collapsed;
            txbkSales.Visibility = Visibility.Collapsed;
        }

        #endregion

        //Show the different UserControl's according to ListViewMenu selection
        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UserControl uc;
            mainGrid.Children.Clear();

            switch (((ListView)sender).SelectedIndex)
            {
                case 0:
                    break;
                //Products
                case 1:
                    uc = new ProductP();
                    mainGrid.Children.Add(uc);
                    
                    break;

                case 2:
                    uc = new EmployeeP();
                    mainGrid.Children.Add(uc);

                    break;

                case 3:
                    uc = new SupplierP();
                    mainGrid.Children.Add(uc);

                    break;

                case 4:
                    uc = new CustomerP();
                    mainGrid.Children.Add(uc);

                    break;

                case 5:

                    break;

                case 6:
                    uc = new SaleP();
                    mainGrid.Children.Add(uc);
                    break;
            }

        }

    }
}
