﻿#pragma checksum "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "FC01384C78A2C6CD557D5288A6EACB6426DDB688"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace SistemaDeVentas {
    
    
    /// <summary>
    /// CustomerPFrm
    /// </summary>
    public partial class CustomerPFrm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeBtn;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock identificationCardTxbk;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox identificationCardMsktxt;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock nameTxbk;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nameTxt;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lastnameTxbk;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lastnameTxt;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock phoneNumberTxbk;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox phoneNumberMsktxt;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock cityTxbk;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox cityTxt;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock departmentTxbk;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox departmentTxt;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock billingAddressTxbk;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox billingAddressTxt;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button agreeBtn;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemaDeVentas;component/customerpresentation/customerpfrm.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
            ((SistemaDeVentas.CustomerPFrm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.closeBtn = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
            this.closeBtn.Click += new System.Windows.RoutedEventHandler(this.CloseBtn_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.identificationCardTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.identificationCardMsktxt = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 5:
            this.nameTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.nameTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.lastnameTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.lastnameTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.phoneNumberTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.phoneNumberMsktxt = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 11:
            this.cityTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.cityTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.departmentTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.departmentTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.billingAddressTxbk = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.billingAddressTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.agreeBtn = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
            this.agreeBtn.Click += new System.Windows.RoutedEventHandler(this.AgreeBtn_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.cancelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\..\CustomerPresentation\CustomerPFrm.xaml"
            this.cancelBtn.Click += new System.Windows.RoutedEventHandler(this.CancelBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

