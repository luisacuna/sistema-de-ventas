
using System.Collections.Generic;

namespace ShowMeTheXAML
{
    public static class XamlDictionary
    {
        static XamlDictionary()
        {
            XamlResolver.Set("njwior", @"<smtx:XamlDisplay Key=""njwior"" HorizontalContentAlignment=""Center"" xmlns:smtx=""clr-namespace:ShowMeTheXAML;assembly=ShowMeTheXAML"">
  <TextBox x:Name=""usernameTxt"" Width=""150"" VerticalAlignment=""Center"" HorizontalAlignment=""Center"" materialDesign:HintAssist.Hint=""Usuario"" Style=""{StaticResource MaterialDesignOutlinedTextFieldTextBox}"" xmlns:materialDesign=""http://materialdesigninxaml.net/winfx/xaml/themes"" xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"" xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"">
    <TextBox.Text>
      <Binding Path=""Name"" UpdateSourceTrigger=""PropertyChanged"">
        <Binding.ValidationRules>
          <local:NotEmptyValidationRule ValidatesOnTargetUpdated=""True"" xmlns:local=""clr-namespace:SistemaDeVentas"" />
        </Binding.ValidationRules>
      </Binding>
    </TextBox.Text>
  </TextBox>
</smtx:XamlDisplay>");
        }
    }
}