﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using _2BusinessLayer;
using System.Data;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for SupplierPFrm.xaml
    /// </summary>
    public partial class SupplierPFrm : Window
    {
        public int SupplierId { get; set; }

        public SupplierPFrm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (SupplierId != 0)
            {
                string answer;
                DataRow dr;

                (dr, answer) = SupplierB.ShowSuppliersByIdAsDataRow(SupplierId);

                if (answer != null)
                {
                    MessageBox.Show("Ha ocurrido un problema al cargar los datos del proveedor: " + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    RUCMsktxt.Text = dr[1].ToString();
                    supplierNameTxt.Text = dr[2].ToString();
                    phoneNumberMsktxt.Text = dr[3].ToString();
                    cityTxt.Text = dr[4].ToString();
                    countryTxt.Text = dr[5].ToString();
                    supplierAddressTxt.Text = dr[6].ToString();
                }
            }
        }

        #region Agree & Cancel button

        private void AgreeBtn_Click(object sender, RoutedEventArgs e)
        {
            //RUCMsktxt.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#03a9f4"));
            //RUCTxbk.Foreground = (Brush)(new BrushConverter().ConvertFrom("#03a9f4"));

            supplierNameTxt.BorderBrush = Brushes.Gray;
            supplierNameTxbk.Foreground = Brushes.Black;

            phoneNumberMsktxt.BorderBrush = Brushes.Gray;
            phoneNumberTxbk.Foreground = Brushes.Black;

            cityTxt.BorderBrush = Brushes.Gray;
            cityTxbk.Foreground = Brushes.Black;

            countryTxt.BorderBrush = Brushes.Gray;
            countryTxbk.Foreground = Brushes.Black;

            supplierAddressTxt.BorderBrush = Brushes.Gray;
            supplierAddressTxbk.Foreground = Brushes.Black;


            if (supplierNameTxt.Text.Equals(string.Empty))
            {
                supplierNameTxt.BorderBrush = Brushes.Red;
                supplierNameTxbk.Foreground = Brushes.Red;

                MessageBox.Show("ERROR! Debe introducir el nombre del proveedor!\n\nDescripción:\nNo se ha introducido el nombre del proveedor\n, " +
                        "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (!phoneNumberMsktxt.IsMaskFull)
            {
                phoneNumberMsktxt.BorderBrush = Brushes.Red;
                phoneNumberTxbk.Foreground = Brushes.Red;

                MessageBox.Show("ERROR! Debe introducir el número de teléfono del proveedor!\n\nDescripción:\nNo se ha introducido número de teléfono del proveedor\n, " +
                        "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (cityTxt.Text.Equals(string.Empty))
            {
                cityTxt.BorderBrush = Brushes.Red;
                cityTxbk.Foreground = Brushes.Red;

                MessageBox.Show("ERROR! Debe introducir la ciudad del proveedor!\n\nDescripción:\nNo se ha introducido la ciudad donde se establece el proveedor\n, " +
                        "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (countryTxt.Text.Equals(string.Empty))
            {
                countryTxt.BorderBrush = Brushes.Red;
                countryTxbk.Foreground = Brushes.Red;

                MessageBox.Show("ERROR! Debe introducir el país de origen del proveedor!\n\nDescripción:\nNo se ha introducido el país de origen del proveedor\n, " +
                        "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (supplierAddressTxt.Text.Equals(string.Empty))
            {
                supplierAddressTxt.BorderBrush = Brushes.Red;
                supplierAddressTxbk.Foreground = Brushes.Red;

                MessageBox.Show("ERROR! Debe introducir la dirección del proveedor!\n\nDescripción:\nNo se ha introducido la dirección del proveedor\n, " +
                        "mientras no lo haga, no puede agregarse el nuevo registro al sistema.", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (SupplierId != 0)
                {
                    string answer;
                    string RUCNumber;

                    if (!RUCMsktxt.IsMaskFull)
                        RUCNumber = "";
                    else
                        RUCNumber = RUCMsktxt.Text;

                    string supplierName = supplierNameTxt.Text;
                    string phoneNumber = phoneNumberMsktxt.Text;
                    string city = cityTxt.Text;
                    string country = countryTxt.Text;
                    string address = supplierAddressTxt.Text;

                    if (!RUCMsktxt.IsMaskFull)
                    {
                        MessageBoxResult result = MessageBox.Show("¿Seguro que no desea almacenar el número RUC del proveedor?\n\nRecomendación:\nSi el proveedor" +
                            " no posee un número de RUC no lo ingrese, de lo contrario por favor introdúzcalo!" +
                            "\n\nEsto puede editarse posteriormente sin ningún problema", "MENSAJE DEL SISTEMA", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    
                        if(result == MessageBoxResult.Yes)
                        {
                            answer = SupplierB.UpdateSupplier(SupplierId, RUCNumber, supplierName, phoneNumber, city, country, address);

                            if (answer == "DONE")
                            {
                                MessageBox.Show("Se ha editado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                                this.DialogResult = true;

                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("ERROR! No se editó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            return;
                        }
                    
                    }

                    //answer = SupplierB.UpdateSupplier(SupplierId, RUCNumber, supplierName, phoneNumber, city, country, address);

                    //if (answer == "DONE")
                    //{
                    //    MessageBox.Show("Se ha editado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    this.DialogResult = true;

                    //    this.Close();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("ERROR! No se editó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    //}
                }
                else
                {
                    string answer;

                    string RUCNumber = RUCMsktxt.Text;
                    string supplierName = supplierNameTxt.Text;
                    string phoneNumber = phoneNumberMsktxt.Text;
                    string city = cityTxt.Text;
                    string country = countryTxt.Text;
                    string address = supplierAddressTxt.Text;

                    if (!RUCMsktxt.IsMaskFull)
                    {
                        MessageBoxResult result = MessageBox.Show("¿Seguro que no desea almacenar el número RUC del proveedor?\n\nRecomendación:\nSi el proveedor" +
                                " no posee un número de RUC no lo ingrese, de lo contrario por favor introdúzcalo!" +
                                "\n\nEsto puede editarse posteriormente sin ningún problema", "MENSAJE DEL SISTEMA", MessageBoxButton.YesNo, MessageBoxImage.Question);

                        if(result == MessageBoxResult.Yes)
                        {
                            answer = SupplierB.InsertSupplier(RUCNumber, supplierName, phoneNumber, city, country, address);

                            if (answer == "DONE")
                            {
                                MessageBox.Show("Se ha ingresado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                                this.DialogResult = true;

                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            return;
                        }

                    }

                    //answer = SupplierB.InsertSupplier(RUCNumber, supplierName, phoneNumber, city, country, address);

                    //if (answer == "DONE")
                    //{
                    //    MessageBox.Show("Se ha ingresado el registro exitosamente!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    this.DialogResult = true;

                    //    this.Close();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("ERROR! No se ingresó el registro!\n\nDescripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    //}
                }
            }

        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e) => CloseBtn_Click(sender, e);

        #endregion

        // CLOSE WINDOW
        private void CloseBtn_Click(object sendr, RoutedEventArgs e) => this.Close();

    }
}
