﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _2BusinessLayer;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for SupplierP.xaml
    /// </summary>
    public partial class SupplierP : UserControl
    {
        private DataTable Suppliers;
        private string Answer;

        public SupplierP()
        {
            InitializeComponent();
        }

        #region Global methods

        private void ShowSuppliers()
        {
            (Suppliers, Answer) = SupplierB.ShowSuppliers();

            if (Answer != null)
            {
                MessageBox.Show("ERROR! No se ha logrado cargar los datos de las ventas!\n\n Descripción:\n" + Answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(Suppliers);

                cancelSearchBtn.Visibility = Visibility.Hidden;
            }
        }

        private void SetDataGridContext(DataTable contextTable)
        {
            if (contextTable == null)
            {
                supplierDG.DataContext = contextTable;
            }
            else
            {
                supplierDG.DataContext = contextTable;
                txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
            }
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ShowSuppliers();
        }

        #region New and update buttons

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView drv = supplierDG.SelectedItem as DataRowView;
                int id = Convert.ToInt32(drv[0].ToString());

                SupplierPFrm spf = new SupplierPFrm
                {
                    SupplierId = id
                };
                bool? result = spf.ShowDialog();

                if (result == true)
                    ShowSuppliers();
            }
            catch
            {
                MessageBox.Show("ERROR! Debe seleccionar una fila para poder editar", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {
            SupplierPFrm spf = new SupplierPFrm();
            bool? result = spf.ShowDialog();

            if (result == true)
                ShowSuppliers();
        }

        #endregion

    }
}
