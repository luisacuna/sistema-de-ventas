﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _2BusinessLayer;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for EmployeeP.xaml
    /// </summary>
    public partial class EmployeeP : UserControl
    {
        private DataTable Employees;
        private string Answer;

        public EmployeeP()
        {
            InitializeComponent();
            Employees = new DataTable();
        }

        #region Global methods

        private void ShowEmployees()
        {
            (Employees, Answer) = EmployeeB.ShowEmployees();

            if (Answer != null)
                MessageBox.Show("Ha ocurrido un problema al cargar los empleados\n\nDescripción:\n" + Answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                SetDataGridContext(Employees);
        }

        private void SetDataGridContext(DataTable contextTable)
        {
            if(contextTable == null)
            {
                employeesDG.DataContext = contextTable;
            }
            else
            {
                employeesDG.DataContext = contextTable;
                txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
            }
        }

        private void DataFilter(string employeeStatus)
        {
            DataTable filteredEmployees;

            try
            {
                filteredEmployees = Employees.Select("[Estado actual] = '" + employeeStatus + "'").CopyToDataTable();

                SetDataGridContext(filteredEmployees);
            }
            catch
            {
                SetDataGridContext(null);
            }
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ShowEmployees();
        }

        private void CmbSearchSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        #region TxtSeeker

        private void TxtSeeker_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void TxtSeeker_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        #endregion

        private void CancelSearchBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        #region RadioButtons All, Active e Inactive Employees

        private void AllEmployeesRdb_Checked(object sender, RoutedEventArgs e)
        {
            cmbSearchSelection.SelectedItem = null;

            MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, "Seleccione un método de búsqueda");
            ShowEmployees();
        }

        private void ActiveEmployeesRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("ACTIVO");
        }

        private void InactiveEmployeesRdb_Checked(object sender, RoutedEventArgs e)
        {
            DataFilter("INACTIVO");
        }


        #endregion

        #region New & Update Employees

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion

    }
}
