﻿using MaterialDesignThemes.Wpf;
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Markup.Localizer;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _2BusinessLayer;
using System.Net.WebSockets;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using System.Linq;

namespace SistemaDeVentas
{
    /// <summary>
    /// Interaction logic for SaleP.xaml
    /// </summary>
    public partial class SaleP : UserControl
    {
        private DataTable Sales;
        private string Answer;

        public SaleP()
        {
            InitializeComponent();
            Sales = new DataTable();
        }

        #region Global methods

        private void ShowSales()
        {
            (Sales, Answer) = SaleB.ShowSales(ShowSaleVersion.Short.ToString());

            if (Answer != null)
            {
                MessageBox.Show("ERROR! No se ha logrado cargar los datos de las ventas!\n\n Descripción:\n" + Answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(Sales);

                cancelSearchBtn.Visibility = Visibility.Hidden;
            }
        }

        private void CheckSearchResults(DataTable results, string answer)
        {
            if (answer != null)
            {
                MessageBox.Show("ERROR! No se ha logrado cargar los datos de las ventas!\n\n Descripción:\n" + answer, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                SetDataGridContext(results);

                //searchResultsRdb.Visibility = Visibility.Visible;
                //searchResultsRdb.IsChecked = true;

                cancelSearchBtn.Visibility = Visibility.Visible;
            }
        }

        private void SetDataGridContext(DataTable contextTable)
        {
            if (contextTable == null)
            {
                salesDG.DataContext = contextTable;
            }
            else
            {
                salesDG.DataContext = contextTable;
                txbkRecordsCount.Text = contextTable.Rows.Count.ToString();
            }
        }

        private void DataFilter(string saleStatus)
        {
            DataTable filteredSales;

            try
            {
                filteredSales = Sales.Select("Estado = '" + saleStatus + "'").CopyToDataTable();

                SetDataGridContext(filteredSales);
            }
            catch
            {
                SetDataGridContext(null);
            }
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            allSalesRdb.IsChecked = true;
        }

        private void CmbSearchSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string hintCaseId = "Buscar venta por código de venta";
            string hintCaseProductId = "Buscar venta por código de producto";
            string hintCaseCustomerId = "Buscar venta por código de producto";

            switch (((ComboBox)sender).SelectedIndex)
            {
                case 0:
                    // Hide everything used to search for sales by dates
                    datesStkp.Visibility = Visibility.Hidden;

                    // Make the TextBox visible and set its respective hint
                    txtSeeker.Clear();
                    txtSeeker.Visibility = Visibility.Visible;
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, hintCaseId);

                    break;
                case 1:
                    // Hide everything required to search sales by dates
                    datesStkp.Visibility = Visibility.Hidden;

                    // Make the TextBox visible and set its respective hint
                    txtSeeker.Clear();
                    txtSeeker.Visibility = Visibility.Visible;
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, hintCaseProductId);

                    break;
                case 2:
                    // Hide everything required to search sales by dates
                    datesStkp.Visibility = Visibility.Hidden;

                    // Make the TextBox visible and set its respective hint
                    txtSeeker.Clear();
                    txtSeeker.Visibility = Visibility.Visible;
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, hintCaseCustomerId);

                    break;
                case 3:
                    // Hide everything required to search sales by Id, Productid & CustomerId
                    txtSeeker.Clear();
                    txtSeeker.Visibility = Visibility.Hidden;

                    // Show everything required to search sales by dates
                    datesStkp.Visibility = Visibility.Visible;

                    // Hide the upper datepicker, because this index searches only by one date
                    upperDateTxbk.Visibility = Visibility.Collapsed;
                    upperDatePicker.Visibility = Visibility.Collapsed;

                    // Change the text in lowerDateTxbk to "Fecha: "
                    lowerDateTxbk.Text = "Fecha: ";

                    break;
                case 4:
                    // Hide everything required to search sales by Id, Productid & CustomerId
                    txtSeeker.Clear();
                    txtSeeker.Visibility = Visibility.Hidden;

                    // Show everything required to search sales by dates
                    datesStkp.Visibility = Visibility.Visible;

                    // Show the top datepicker, because this index searches by two dates
                    upperDateTxbk.Visibility = Visibility.Visible;
                    upperDatePicker.Visibility = Visibility.Visible;

                    // Change the text in lowerDateTxbk to "Fecha inicial: "
                    lowerDateTxbk.Text = "Fecha inicial: ";
                    break;
            }

        }

        #region AllSales, SalesWithGuarateeMade, RefundedSales

        private void AllSalesRdb_Checked(object sender, RoutedEventArgs e)
        {
            if (cancelSearchBtn.IsVisible)
            {
                SetDataGridContext(Sales);
            }
            else
            {
                MaterialDesignThemes.Wpf.HintAssist.SetHint(txtSeeker, "Seleccione un método de búsqueda");
                ShowSales();
            }
        }

        private void ApprovedSales_Checked(object sender, RoutedEventArgs e)
        {
            string saleStatus = "APROBADO";

            DataFilter(saleStatus);
        }

        private void SalesWithGuaranteeMadeRdb_Checked(object sender, RoutedEventArgs e)
        {
            string saleStatus = "GARANTÍA";

            DataFilter(saleStatus);
        }

        private void RefundedSalesRdb_Checked(object sender, RoutedEventArgs e)
        {
            string saleStatus = "DEVOLUCIÓN";

            DataFilter(saleStatus);
        }

        #endregion    

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            lowerDatePicker.BorderBrush = Brushes.Transparent;
            upperDatePicker.BorderBrush = Brushes.Transparent;

            try
            {
                if (cmbSearchSelection.SelectedIndex.Equals(3))
                {
                    if ((lowerDatePicker.SelectedDate == null) || (lowerDatePicker.Text == null))
                    {
                        lowerDatePicker.BorderBrush = Brushes.Red;
                        MessageBox.Show("ERROR! Debe seleccionar una fecha para poder realizar la búsqueda!", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        DateTime date = (DateTime)lowerDatePicker.SelectedDate;

                        (Sales, Answer) = SaleB.ShowSalesByDate(date);

                        CheckSearchResults(Sales, Answer);
                    }
                }
                if (cmbSearchSelection.SelectedIndex.Equals(4))
                {
                    if ((lowerDatePicker.SelectedDate == null) || (lowerDatePicker.Text == null))
                    {
                        lowerDatePicker.BorderBrush = Brushes.Red;
                        MessageBox.Show("ERROR! Debe seleccionar la fecha inicial en el intervalo", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if ((upperDatePicker.SelectedDate == null) || (upperDatePicker.Text == null))
                    {
                        upperDatePicker.BorderBrush = Brushes.Red;
                        MessageBox.Show("ERROR! Debe seleccionar la fecha inicial en el intervalo", "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        DateTime lowerDate = (DateTime)lowerDatePicker.SelectedDate;
                        DateTime upperDate = (DateTime)upperDatePicker.SelectedDate;

                        (Sales, Answer) = SaleB.ShowSalesBetweenDates(lowerDate, upperDate);

                        CheckSearchResults(Sales, Answer);
                    }
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("ERROR! Debe seleccionar un índice de búsqueda!\n\nDescripción:\n" + ex.Message, "MENSAJE DEL SISTEMA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CancelSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            allSalesRdb.IsChecked = true;
            ShowSales();

            cancelSearchBtn.Visibility = Visibility.Hidden;

            txtSeeker.Clear();
            lowerDatePicker.SelectedDate = null;
            upperDatePicker.SelectedDate = null;
        }
    }
}
