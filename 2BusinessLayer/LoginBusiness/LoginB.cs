﻿using System;
using System.Collections.Generic;
using System.Text;
using _1DataLayer;

namespace _2BusinessLayer
{
    public class LoginB
    {
        public static (int, string) AppLogin_Verifier(string username, string password)
        {
            return new LoginD { Username = username, Password = password }.AppLogin_Verifier();
        }
    }
}
