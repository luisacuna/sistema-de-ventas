﻿using _1DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace _2BusinessLayer
{
    public class SaleB
    {
        // SHOW SALES
        public static (DataTable, string) ShowSales(string version) => new SaleD { Version = version }.Show();

        // SHOW SALES BY STATUS
        public static (DataTable, string) ShowSalesByStatus(string saleStatus) => new SaleD { SaleStatus = saleStatus }.ShowSalesByStatus();

        // SHOW SALES BY DATE
        public static (DataTable, string) ShowSalesByDate(DateTime date) => new SaleD { SaleDate = date }.ShowSalesByDate();

        // SHOW SALES BETWEEN DATES
        public static (DataTable, string) ShowSalesBetweenDates(DateTime lowerDate, DateTime upperDate) => new SaleD().ShowSalesBetweenDates(lowerDate, upperDate);
    }
}