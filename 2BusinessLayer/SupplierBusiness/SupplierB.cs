﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using _1DataLayer;

namespace _2BusinessLayer
{
    public class SupplierB
    {
        // INSERT
        public static string InsertSupplier(string RUCNumber, string supplierName, string phoneNumber,
            string city, string country, string supplierAddress)
        {
            return new SupplierD
            {
                RUC = RUCNumber,
                SupplierName = supplierName,
                PhoneNumber = phoneNumber,
                City = city,
                Country = country,
                SupplierAddress = supplierAddress
            }.Insert();
        }

        // UPDATE
        public static string UpdateSupplier(int idSupplier, string RUCNumber, string supplierName, string phoneNumber,
            string city, string country, string supplierAddress)
        {
            return new SupplierD
            {
                SupplierId = idSupplier,
                RUC = RUCNumber,
                SupplierName = supplierName,
                PhoneNumber = phoneNumber,
                City = city,
                Country = country,
                SupplierAddress = supplierAddress
            }.Update();
        }

        // SHOW
        public static (DataTable, string) ShowSuppliers() => new SupplierD().Show();

        // SHOW BY ID
        public static (DataTable, string) ShowSuppliersById(int idSupplier) => new SupplierD { SupplierId = idSupplier }.ShowById();

        // SHOW SUPPLIER BY ID AS DATAROW
        public static (DataRow, string) ShowSuppliersByIdAsDataRow(int idSupplier)
        {
            DataRow dataRow;

            DataTable dataTable;
            string answer;

            (dataTable, answer) = ShowSuppliersById(idSupplier);

            if (answer != null)
                dataRow = null;
            else
                dataRow = dataTable.Rows[0];


            return (dataRow, answer);
        }

    }
}
