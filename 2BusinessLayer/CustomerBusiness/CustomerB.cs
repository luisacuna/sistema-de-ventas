﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using _1DataLayer;
using System.Runtime.InteropServices;

namespace _2BusinessLayer
{
    public class CustomerB
    {
        //INSERT
        public static string InsertCustomer(string identificationCard, string name, string lastname, string phoneNumber, 
            string billingAddress, string city, string department)
        {
            return new CustomerD(identificationCard, name, lastname, phoneNumber, billingAddress, city, department).Insert();
        }

        //UPDATE
        public static string UpdateCustomer(int customerId, string identificationCard, string name, string lastname, string phoneNumber,
            string billingAddress, string city, string department)
        {
            return new CustomerD(customerId, identificationCard, name, lastname, phoneNumber, billingAddress, city, department).Update();
        }

        // SHOW CUSTOMERS
        public static (DataTable, string) ShowCustomers() => new CustomerD().Show();

        // SHOW CUSTOMER BY STATE
        public static (DataTable, string) ShowCustomerByState(string state) => new CustomerD().ShowCustomerByState(state);

        // SHOW CUSTOMER BY ID
        public static (DataTable, string) ShowCustomerById(int id, int mode) => new CustomerD { CustomerId = id, ModeToShowById = mode }.ShowById();

        //SHOW CUSTOMER DATA BY ID AS DATAROW
        public static (DataRow, string) CustomerByIdAsDataRow(int id, int mode)
        {
            DataRow dr;

            DataTable dataTable;
            string answer;

            (dataTable, answer) = ShowCustomerById(id, mode);

            if (answer != null)
            {
                dr = null;
            }
            else
            {
                dr = dataTable.Rows[0];
            }

            return (dr, answer);
        }

        // SHOW CUSTOMER BY IDENTIFICATION CARD
        public static (DataTable, string) ShowCustomerByIdentificationCard(string identificationCard)
        {
            return new CustomerD { IdentificationCard = identificationCard }.ShowCustomerByIdentificationCard();
        }

        // SHOW CUSTOMER BY PHONE NUMBER
        public static (DataTable, string) ShowCustomerByPhoneNumber(string phoneNumber)
        {
            return new CustomerD { PhoneNumber = phoneNumber }.ShowCustomerByPhoneNumber();
        }

        // SHOW CUSTOMER BY NAME OR LASTNAME
        public static (DataTable, string) ShowCustomersByNameOrLastname(string nameOrLastname)
        {
            return new CustomerD().ShowCustomersByNameOrLastname(nameOrLastname);
        }

        // SHOW CUSTOMERS BY CITY
        public static (DataTable, string) ShowCustomersByCity(string city) => new CustomerD { City = city }.ShowCustomerByCity();

        // SHOW CUSTOMERS BY DEPARTMENT
        public static (DataTable, string) ShowCustomersByBillingAddres(string billlingAddress)
        {
            return new CustomerD { BillingAddress = billlingAddress }.ShowCustomerByBillingAddress();
        }
    }
}
