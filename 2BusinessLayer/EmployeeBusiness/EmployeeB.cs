﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using _1DataLayer;

namespace _2BusinessLayer
{
    public class EmployeeB
    {
        // INSERT EMPLOYEE 
        public static string InsertEmployee(string iNSSNumber, string identificationCard, string employeeName, string employeeLastname, 
            string phoneNumber, int workAreaId, string homeAddress, string city, string department, string employeeStatus)
        {
            return new EmployeeD(iNSSNumber, identificationCard, employeeName, employeeLastname, phoneNumber, workAreaId, homeAddress, city, department, employeeStatus).Insert();
        }

        // UPDATE EMPLOYEE 
        public static string UpdateEmployee(int idemployee, string iNSSNumber, string identificationCard, string employeeName, string employeeLastname,
            string phoneNumber, int workAreaId, string homeAddress, string city, string department, string employeeStatus)
        {
            return new EmployeeD(idemployee, iNSSNumber, identificationCard, employeeName, employeeLastname, phoneNumber, workAreaId, homeAddress, city, department, employeeStatus).Insert();
        }

        // SHOW ALL THE EMPLOYEES
        public static (DataTable, string) ShowEmployees() => new EmployeeD().Show();

        // SHOW EMPLOYEE BY ID
        public static (DataTable, string) ShowEmployeeById(int idEmployee) => new EmployeeD { EmployeeId = idEmployee }.ShowById();

        // SHOW EMPLOYEE BY ID AS DATAROW
        public static DataRow ShowEmployeeByIdAsDataRow(int idEmployee)
        {
            string answer;
            DataRow dataRow;

            DataTable dataTable;

            (dataTable, answer) = ShowEmployeeById(idEmployee);

            if (answer != null)
                dataRow = null;
            else
                dataRow = dataTable.Rows[0];

            return dataRow;
        }

    }
}