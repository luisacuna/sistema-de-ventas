﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using _1DataLayer;

namespace _2BusinessLayer
{
    public class ProductCategoryB
    {
        //INSERT
        public static string InsertProductCategory(string categoryName) => new ProductCategoryD(categoryName).Insert();

        //UPDATE
        public static string UpdateProductCategory(int idCategory, string categoryName) => new ProductCategoryD(idCategory, categoryName).Update();

        //SHOW
        public static (DataTable, string) ShowProductCategories() => new ProductCategoryD().Show();

        //SHOW PRODUCT CATEGORY BY ID
        public static (DataTable, string) ShowProductCategoryById(int idCategory) => new ProductCategoryD { ProductCategoryId = idCategory }.ShowById();

        //SHOW PRODUCT CATEGORY BY ID AS DATAROW
        public static (DataRow, string) ProductCategoryByIdAsDataRow(int idCategory)
        {
            DataRow dr;
            DataTable dataTable;
            string answer;

            (dataTable, answer) = ShowProductCategoryById(idCategory);
            
            if(answer != null)
            {
                dr = null;
            }
            else
            {
                dr = dataTable.Rows[0];
            }

            return (dr, answer);
        }

        // SHOW PRODUCTS BELONGING TO A CATEGORY
        public static int ProductCountOfACategory(int categoryId)
        {
            DataTable dataTable;
            int productCount;
            string answer;

            (dataTable, answer) = ProductB.ShowProductsByCategoryId(categoryId);

            if (answer != null)
            {
                productCount = 0;
            }
            else
            {
                productCount = dataTable.Rows.Count;
            }

            return productCount;
        }

    }
}
