﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using _1DataLayer;

namespace _2BusinessLayer
{
    public class ProductB
    {        
        // INSERT
        public static string InsertProduct(int idSupplier, string productName, string productDescription, int idCategory)
        {
            return new ProductD(idSupplier, productName, productDescription, idCategory).Insert();
        }

        // UPDATE
        public static string UpdateProduct(int idProduct, int idSupplier, string productName, string productDescription, int idCategory)
        {
            return new ProductD(idProduct, idSupplier, productName, productDescription, idCategory).Update();
        }

        // SHOW
        public static (DataTable, string) ShowProducts() => new ProductD().Show();

        // SHOW PRODUCTS BY ID
        public static (DataTable, string) ShowProductById(int id) => new ProductD { ProductId = id }.ShowById();

        // Show products by category Id
        public static (DataTable, string) ShowProductsByCategoryId(int idCategory) => new ProductD().ShowProductsByCategoryId(idCategory);

        // Show products by category Id
        public static (DataTable, string) ShowProductBySupplierId(int idSupplier) => new ProductD().ShowProductsBySupplierId(idSupplier);

        // Show products by Availability
        public static (DataTable, string) ShowProductByAvailability(string availability) => new ProductD().ShowProductByAvailability(availability);

        public static (DataRow, string) ProductByIdAsDataRow(int id)
        {
            DataRow dr;

            DataTable dataTable;
            string answer;

            (dataTable, answer) = ShowProductById(id);

            if(answer != null)
            {
                dr = null;
            }
            else
            {
                dr = dataTable.Rows[0];
            }

            return (dr, answer);
        }

        // SHOW PRODUCTS BY NAME
        public static (DataTable, string) ShowProductByName(string name) => new ProductD().ShowProductByName(name);
    }
}