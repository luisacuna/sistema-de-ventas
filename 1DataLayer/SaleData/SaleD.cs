﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class SaleD : ISIUD
    {
        public int SaleId { get; set; }
        public DateTime SaleDate { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string SaleStatus { get; set; }
        public string Version { get; set; } //Required to show Sales in database, ...
                                                //Version must be equals to "Short" or "Long"

        public SaleD()
        {
        }

         //TO UPDATE
        public SaleD(int saleId, DateTime saleDate, int employeeId, int customerId, string saleStatus)
        {
            SaleId = saleId;
            SaleDate = saleDate;
            EmployeeId = employeeId;
            CustomerId = customerId;
            SaleStatus = saleStatus;
        }

        //TO INSERT
        public SaleD(DateTime saleDate, int employeeId, int customerId, string saleStatus)
        {
            SaleDate = saleDate;
            CustomerId = customerId;
            EmployeeId = employeeId;
            SaleStatus = saleStatus;
        }

        public string Insert()
        {
            throw new NotImplementedException();
        }

        public string Update()
        {
            throw new NotImplementedException();
        }

        // SHOW SALES
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_Sale";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter sVersion = new SqlParameter
                {
                    ParameterName = "@version", SqlDbType = SqlDbType.VarChar, Size = 10, Value = Version
                };
                command.Parameters.Add(sVersion);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW SALES BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_SaleById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter idSale = new SqlParameter
                {
                    ParameterName = "@idSale", SqlDbType = SqlDbType.Int, Value = SaleId
                };
                command.Parameters.Add(idSale);

                SqlParameter sVersion = new SqlParameter
                {
                    ParameterName = "@version", SqlDbType = SqlDbType.VarChar, Size = 10, Value = Version
                };
                command.Parameters.Add(sVersion);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW SALES BY STATUS
        public (DataTable, string) ShowSalesByStatus()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_SaleByStatus";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter status = new SqlParameter
                {
                    ParameterName = "@saleStatus", SqlDbType = SqlDbType.VarChar, Size = 10, Value = SaleStatus
                };
                command.Parameters.Add(status);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW SALES BY DATE
        public (DataTable, string) ShowSalesByDate()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_SalesByDate";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter saleDate = new SqlParameter
                {
                    ParameterName = "@date", SqlDbType = SqlDbType.Date, Value = SaleDate.Date
                };
                command.Parameters.Add(saleDate);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch(Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW SALES BETWEEN DATES
        public (DataTable, string) ShowSalesBetweenDates(DateTime lowerDate, DateTime upperDate)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_SalesBetweenDates";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter lower = new SqlParameter
                {
                    ParameterName = "@lowerDate", SqlDbType = SqlDbType.Date, Value = lowerDate
                };
                command.Parameters.Add(lower);

                SqlParameter upper = new SqlParameter
                {
                    ParameterName = "@upperDate", SqlDbType = SqlDbType.Date, Value = upperDate
                };
                command.Parameters.Add(upper);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }
    }
}
