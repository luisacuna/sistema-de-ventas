﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace _1DataLayer
{
    interface ISIUD
    {
        string Insert();
        string Update();
        (DataTable, string) Show();
        (DataTable, string) ShowById();
    }
}
