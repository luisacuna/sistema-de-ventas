﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class EmployeeD : ISIUD
    {
        public int EmployeeId { get; set; }
        public string INSSNumber { get; set; }
        public string IdentificationCard { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeLastname { get; set; }
        public string PhoneNumber { get; set; }
        public int WorkAreaId { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime DismissalDate { get; set; }
        public string EmployeeStatus { get; set; }

        public EmployeeD()
        {
        }

        // TO UPDATE
        public EmployeeD(int employeeId, string iNSSNumber, string identificationCard, string employeeName, 
            string employeLastname, string phoneNumber, int workAreaId, string homeAddress, string city, 
            string department, string employeeStatus)
        {
            EmployeeId = employeeId;
            INSSNumber = iNSSNumber;
            IdentificationCard = identificationCard;
            EmployeeName = employeeName;
            EmployeLastname = employeLastname;
            PhoneNumber = phoneNumber;
            WorkAreaId = workAreaId;
            HomeAddress = homeAddress;
            City = city;
            Department = department;
            EmployeeStatus = employeeStatus;
        }

        // TO INSERT
        public EmployeeD(string iNSSNumber, string identificationCard, string employeeName, string employeeLastname, 
            string phoneNumber, int workAreaId, string homeAddress, string city, string department, string employeeStatus)
        {
            INSSNumber = iNSSNumber;
            IdentificationCard = identificationCard;
            EmployeeName = employeeName;
            EmployeLastname = employeeLastname;
            PhoneNumber = phoneNumber;
            WorkAreaId = workAreaId;
            HomeAddress = homeAddress;
            City = city;
            Department = department;
            EmployeeStatus = employeeStatus;
        }

        // TO INSERT
        public string Insert()
        {
            string answer = null;
            string sqlQuery = "I_Employee";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter iNSSNumber = new SqlParameter
                {
                    ParameterName = "@iNSSNumber", SqlDbType = SqlDbType.VarChar, Size = 9, Value = INSSNumber
                };
                command.Parameters.Add(iNSSNumber);

                SqlParameter identificationCard = new SqlParameter
                {
                    ParameterName = "@identificationCard", SqlDbType = SqlDbType.VarChar, Size = 16, Value = IdentificationCard
                };
                command.Parameters.Add(identificationCard);

                SqlParameter employeeName = new SqlParameter
                {
                    ParameterName = "@employeeName", SqlDbType = SqlDbType.VarChar, Size = 60, Value = EmployeeName
                };
                command.Parameters.Add(employeeName);

                SqlParameter employeeLastname = new SqlParameter
                {
                    ParameterName = "@employeeLastname", SqlDbType = SqlDbType.VarChar, Size = 60, Value = EmployeLastname
                };
                command.Parameters.Add(employeeLastname);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber", SqlDbType = SqlDbType.VarChar, Size = 20, Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter idWorkArea = new SqlParameter
                {
                    ParameterName = "@idWorkArea", SqlDbType = SqlDbType.Int, Value = WorkAreaId
                };
                command.Parameters.Add(idWorkArea);

                SqlParameter homeAddress = new SqlParameter
                {
                    ParameterName = "@homeAddress", SqlDbType = SqlDbType.VarChar, Size = 150, Value = HomeAddress
                };
                command.Parameters.Add(homeAddress);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city", SqlDbType = SqlDbType.VarChar, Size = 50, Value = City
                };
                command.Parameters.Add(city);

                SqlParameter department = new SqlParameter
                {
                    ParameterName = "@department", SqlDbType = SqlDbType.VarChar, Size = 50, Value = Department
                };
                command.Parameters.Add(department);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }
        
        // TO UPDATE
        public string Update()
        {
            string answer = null;
            string sqlQuery = "U_Employee";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter idEmployee = new SqlParameter
                {
                    ParameterName = "@idEmployee", SqlDbType = SqlDbType.Int, Value = EmployeeId
                };
                command.Parameters.Add(idEmployee);

                SqlParameter iNSSNumber = new SqlParameter
                {
                    ParameterName = "@iNSSNumber", SqlDbType = SqlDbType.VarChar, Size = 9, Value = INSSNumber
                };
                command.Parameters.Add(iNSSNumber);

                SqlParameter identificationCard = new SqlParameter
                {
                    ParameterName = "@identificationCard", SqlDbType = SqlDbType.VarChar, Size = 16, Value = IdentificationCard
                };
                command.Parameters.Add(identificationCard);

                SqlParameter employeeName = new SqlParameter
                {
                    ParameterName = "@employeeName", SqlDbType = SqlDbType.VarChar, Size = 60, Value = EmployeeName
                };
                command.Parameters.Add(employeeName);

                SqlParameter employeeLastname = new SqlParameter
                {
                    ParameterName = "@employeeLastname", SqlDbType = SqlDbType.VarChar, Size = 60, Value = EmployeLastname
                };
                command.Parameters.Add(employeeLastname);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber", SqlDbType = SqlDbType.VarChar, Size = 20, Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter idWorkArea = new SqlParameter
                {
                    ParameterName = "@idWorkArea", SqlDbType = SqlDbType.Int, Value = WorkAreaId
                };
                command.Parameters.Add(idWorkArea);

                SqlParameter homeAddress = new SqlParameter
                {
                    ParameterName = "@homeAddress", SqlDbType = SqlDbType.VarChar, Size = 150, Value = HomeAddress
                };
                command.Parameters.Add(homeAddress);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city", SqlDbType = SqlDbType.VarChar, Size = 50, Value = City
                };
                command.Parameters.Add(city);

                SqlParameter department = new SqlParameter
                {
                    ParameterName = "@department", SqlDbType = SqlDbType.VarChar, Size = 50, Value = Department
                };
                command.Parameters.Add(department);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        // SHOW
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_Employee";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_EmployeeById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter idEmployee = new SqlParameter
                {
                    ParameterName = "@idEmployee", SqlDbType = SqlDbType.Int, Value = EmployeeId
                };
                command.Parameters.Add(idEmployee);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }
        



    }
}
