﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class SupplierD : ISIUD
    {
        public int SupplierId { get; set; }
        public string RUC { get; set; }
        public string SupplierName { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string SupplierAddress { get; set; }

        public SupplierD() { }

        // To Update
        public SupplierD(int idSuplier, string rUC, string supplierName, string phoneNumber,
            string city, string country, string supplierAddress)
        {
            SupplierId = idSuplier;
            RUC = rUC;
            SupplierName = supplierName;
            PhoneNumber = phoneNumber;
            City = city;
            Country = country;
            SupplierAddress = supplierAddress;
        }

        // To Insert
        public SupplierD(string rUC, string supplierName, string phoneNumber, string city, string country, string supplierAddress)
        {
            RUC = rUC;
            SupplierName = supplierName;
            PhoneNumber = phoneNumber;
            City = city;
            Country = country;
            SupplierAddress = supplierAddress;
        }

        // INSERT SUPPLIER
        public string Insert()
        {
            string answer = null;
            string sqlQuery = "I_Supplier";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQuery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter rucNumber = new SqlParameter
                {
                    ParameterName = "@RUC",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = RUC
                };
                command.Parameters.Add(rucNumber);

                SqlParameter supplierName = new SqlParameter
                {
                    ParameterName = "@supplierName",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 60,
                    Value = SupplierName
                };
                command.Parameters.Add(supplierName);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20,
                    Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 40,
                    Value = Country
                };
                command.Parameters.Add(city);

                SqlParameter country = new SqlParameter
                {
                    ParameterName = "@country",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 40,
                    Value = Country
                };
                command.Parameters.Add(country);

                SqlParameter address = new SqlParameter
                {
                    ParameterName = "@supplierAddress",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 150,
                    Value = SupplierAddress
                };
                command.Parameters.Add(address);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";
            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        // UPDATE SUPPLIER
        public string Update()
        {
            string answer = null;
            string sqlQuery = "U_Supplier";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQuery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter idSupplier = new SqlParameter
                {
                    ParameterName = "@idSupplier",
                    SqlDbType = SqlDbType.Int,
                    Value = SupplierId
                };
                command.Parameters.Add(idSupplier);

                SqlParameter rucNumber = new SqlParameter
                {
                    ParameterName = "@RUC",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = RUC
                };
                command.Parameters.Add(rucNumber);

                SqlParameter supplierName = new SqlParameter
                {
                    ParameterName = "@supplierName",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 60,
                    Value = SupplierName
                };
                command.Parameters.Add(supplierName);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20,
                    Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 40,
                    Value = Country
                };
                command.Parameters.Add(city);

                SqlParameter country = new SqlParameter
                {
                    ParameterName = "@country",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 40,
                    Value = Country
                };
                command.Parameters.Add(country);

                SqlParameter address = new SqlParameter
                {
                    ParameterName = "@supplierAddress",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 150,
                    Value = SupplierAddress
                };
                command.Parameters.Add(address);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";
            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        // SHOW
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_Supplier";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW SUPPLIER BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_SupplierById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQuery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter idSupplier = new SqlParameter
                {
                    ParameterName = "@idSupplier",
                    SqlDbType = SqlDbType.Int,
                    Value = SupplierId
                };
                command.Parameters.Add(idSupplier);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }
    }
}
