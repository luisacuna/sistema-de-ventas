﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class ProductD : ISIUD
    {
        public int ProductId { get; set; }
        public int SupplierId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int CategoryId { get; set; }

        public ProductD() { }

        //TO UPDATE
        public ProductD(int idProduct, int idSupplier, string productName, string productDescription, int idCategory)
        {
            ProductId = idProduct;
            SupplierId = idSupplier;
            ProductName = productName;
            ProductDescription = productDescription;
            CategoryId = idCategory;
        }

        //TO INSERT
        public ProductD(int idSupplier, string productName, string productDescription, int idCategory)
        {
            SupplierId = idSupplier;
            ProductName = productName;
            ProductDescription = productDescription;
            CategoryId = idCategory;
        }

        //INSERT PRODUCT
        public string Insert()
        {
            string answer = null;
            string sqlQwery = "I_Product";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQwery, CommandType = CommandType.StoredProcedure };

                SqlParameter idSupplier = new SqlParameter { ParameterName = "@idSupplier", SqlDbType = SqlDbType.Int, Value = SupplierId };
                command.Parameters.Add(idSupplier);

                SqlParameter productName = new SqlParameter
                {
                    ParameterName = "@productName", SqlDbType = SqlDbType.VarChar, Size = 100,
                    Value = ProductName
                };
                command.Parameters.Add(productName);

                SqlParameter productDescription = new SqlParameter
                {
                    ParameterName = "@productDescription", SqlDbType = SqlDbType.VarChar, Size = 200,
                    Value = ProductDescription
                };
                command.Parameters.Add(productDescription);

                SqlParameter idCategory = new SqlParameter { ParameterName = "@idCategory", SqlDbType = SqlDbType.Int, Value = CategoryId };
                command.Parameters.Add(idCategory);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        //UPDATE PRODUCT
        public string Update()
        {
            string answer = null;
            string sqlQuery = "U_Product";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter idProduct = new SqlParameter { ParameterName = "@idProduct", SqlDbType = SqlDbType.Int, Value = ProductId };
                command.Parameters.Add(idProduct);

                SqlParameter idSupplier = new SqlParameter { ParameterName = "@idSupplier", SqlDbType = SqlDbType.Int, Value = SupplierId };
                command.Parameters.Add(idSupplier);

                SqlParameter productName = new SqlParameter { ParameterName = "@productName", SqlDbType = SqlDbType.VarChar, Size = 100, Value = ProductName };
                command.Parameters.Add(productName);
                SqlParameter productDescription = new SqlParameter { ParameterName = "@productDescription", SqlDbType = SqlDbType.VarChar, Size = 200, Value = ProductDescription };
                command.Parameters.Add(productDescription);

                SqlParameter idCategory = new SqlParameter { ParameterName = "@idCategory", SqlDbType = SqlDbType.Int, Value = CategoryId };
                command.Parameters.Add(idCategory);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        //SHOW PRODUCT
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_Product";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCT BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter productId = new SqlParameter { ParameterName = "@idProduct", SqlDbType = SqlDbType.Int, Value = ProductId };
                command.Parameters.Add(productId);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCTS BY CATEGORY ID
        public (DataTable, string) ShowProductsByCategoryId(int idCategory)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductByCategoryId";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };
                
                SqlParameter categoryId = new SqlParameter { ParameterName = "@idCategory", SqlDbType = SqlDbType.Int, Value = idCategory };
                command.Parameters.Add(categoryId);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCTS BY SUPPLIER ID
        public (DataTable, string) ShowProductsBySupplierId(int idSupplier)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductBySupplierId";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter supplierId = new SqlParameter { ParameterName = "@idSupplier", SqlDbType = SqlDbType.Int, Value = idSupplier };
                command.Parameters.Add(supplierId);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCTS BY AVAILABILITY
        public (DataTable, string) ShowProductByAvailability(string availability)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductByAvailability";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter productAvailability = new SqlParameter { ParameterName = "@availability", SqlDbType = SqlDbType.VarChar, Size = 10, 
                    Value = availability };
                command.Parameters.Add(productAvailability);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCTS BY NAME
        public (DataTable, string) ShowProductByName(string name)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductByName";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter productName = new SqlParameter { ParameterName = "@productName", SqlDbType = SqlDbType.VarChar, Size = 100, Value = name };
                command.Parameters.Add(productName);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

    }
}
