﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class ProductCategoryD : ISIUD
    {
        public int ProductCategoryId { get; set; }
        public string CategoryName { get; set; }

        #region Builders

        public ProductCategoryD() { }

        // TO UPDATE
        public ProductCategoryD(int idCategory, string categoryName)
        {
            ProductCategoryId = idCategory;
            CategoryName = categoryName;
        }

        // TO INSERT
        public ProductCategoryD(string categoryName)
        {
            CategoryName = categoryName;
        }

        #endregion

        // INSERT PRODUCT CATEGORY
        public string Insert()
        {
            string answer = null;
            string sqlQwery = "I_ProductCategory";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQwery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter productName = new SqlParameter
                {
                    ParameterName = "@categoryName",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = CategoryName
                };
                command.Parameters.Add(productName);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        //UPDATE PRODUCT CATEGORY
        public string Update()
        {
            string answer = null;
            string sqlQwery = "U_ProductCategory";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQwery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter idCategory = new SqlParameter
                {
                    ParameterName = "@idCategory",
                    SqlDbType = SqlDbType.Int,
                    Value = ProductCategoryId
                };
                command.Parameters.Add(idCategory);

                SqlParameter productName = new SqlParameter
                {
                    ParameterName = "@categoryName",
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = CategoryName
                };
                command.Parameters.Add(productName);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        //SHOW PRODUCT CATEGORIES BY ID
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductCategory";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQuery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        //SHOW PRODUCT CATEGORIES BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ProductCategoryById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = sqlQuery,
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter categoryId = new SqlParameter
                {
                    ParameterName = "@idCategory",
                    SqlDbType = SqlDbType.Int,
                    Value = ProductCategoryId
                };
                command.Parameters.Add(categoryId);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }
    }
}
