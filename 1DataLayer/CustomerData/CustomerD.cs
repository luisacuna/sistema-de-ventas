﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace _1DataLayer
{
    public class CustomerD : ISIUD
    {
        public int CustomerId { get; set; }
        public string IdentificationCard { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string PhoneNumber { get; set; }
        public string BillingAddress { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public int ModeToShowById { get; set; }

        #region Builders

        public CustomerD() { }

        //TO UPDATE
        public CustomerD(int customerId, string identificationCard, string name, string lastname, string phoneNumber, string billingAddress, string city, string department)
        {
            CustomerId = customerId;
            IdentificationCard = identificationCard;
            Name = name;
            Lastname = lastname;
            PhoneNumber = phoneNumber;
            BillingAddress = billingAddress;
            City = city;
            Department = department;
        }

        //TO INSERT
        public CustomerD(string identificationCard, string name, string lastname, string phoneNumber, string billingAddress, string city, string department)
        {
            IdentificationCard = identificationCard;
            Name = name;
            Lastname = lastname;
            PhoneNumber = phoneNumber;
            BillingAddress = billingAddress;
            City = city;
            Department = department;
        }

        #endregion

        // INSERT CUSTOMER
        public string Insert()
        {
            string answer = null;
            string sqlQwery = "I_Customer";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQwery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter identificationCard = new SqlParameter
                {
                    ParameterName = "@identificationCard", SqlDbType = SqlDbType.VarChar, Size = 16, Value = IdentificationCard
                };
                command.Parameters.Add(identificationCard);

                SqlParameter customerName = new SqlParameter
                {
                    ParameterName = "@customerName", SqlDbType = SqlDbType.VarChar, Size = 60, Value = Name
                };
                command.Parameters.Add(customerName);

                SqlParameter customerLastname = new SqlParameter
                {
                    ParameterName = "@customerLastname", SqlDbType = SqlDbType.VarChar, Size = 60, Value = Lastname
                };
                command.Parameters.Add(customerLastname);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber", SqlDbType = SqlDbType.VarChar, Size = 20, Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter billingAddress = new SqlParameter
                {
                    ParameterName = "@billingAddress", SqlDbType = SqlDbType.VarChar, Size = 150, Value = BillingAddress
                };
                command.Parameters.Add(billingAddress);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city", SqlDbType = SqlDbType.VarChar, Size = 50, Value = City
                };
                command.Parameters.Add(city);

                SqlParameter department = new SqlParameter
                {
                    ParameterName = "@department", SqlDbType = SqlDbType.VarChar, Size = 50, Value = Department
                };
                command.Parameters.Add(department);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;

        }

        // UPDATE CUSTOMER
        public string Update()
        {
            string answer = null;
            string sqlQwery = "U_Customer";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQwery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter customerId = new SqlParameter
                {
                    ParameterName = "@idCustomer", SqlDbType = SqlDbType.Int, Value = CustomerId
                };
                command.Parameters.Add(customerId);

                SqlParameter identificationCard = new SqlParameter
                {
                    ParameterName = "@identificationCard", SqlDbType = SqlDbType.VarChar, Size = 16, Value = IdentificationCard
                };
                command.Parameters.Add(identificationCard);

                SqlParameter customerName = new SqlParameter
                {
                    ParameterName = "@customerName", SqlDbType = SqlDbType.VarChar, Size = 60, Value = Name
                };
                command.Parameters.Add(customerName);

                SqlParameter customerLastname = new SqlParameter
                {
                    ParameterName = "@customerLastname", SqlDbType = SqlDbType.VarChar, Size = 60, Value = Lastname
                };
                command.Parameters.Add(customerLastname);

                SqlParameter phoneNumber = new SqlParameter
                {
                    ParameterName = "@phoneNumber", SqlDbType = SqlDbType.VarChar, Size = 20, Value = PhoneNumber
                };
                command.Parameters.Add(phoneNumber);

                SqlParameter billingAddress = new SqlParameter
                {
                    ParameterName = "@billingAddress", SqlDbType = SqlDbType.VarChar, Size = 150, Value = BillingAddress
                };
                command.Parameters.Add(billingAddress);

                SqlParameter city = new SqlParameter
                {
                    ParameterName = "@city", SqlDbType = SqlDbType.VarChar, Size = 50, Value = City
                };
                command.Parameters.Add(city);

                SqlParameter department = new SqlParameter
                {
                    ParameterName = "@department", SqlDbType = SqlDbType.VarChar, Size = 50, Value = Department
                };
                command.Parameters.Add(department);

                answer = command.ExecuteNonQuery() == 1 ? "DONE" : "THERE WAS A PROBLEM";

            }
            catch (Exception ex)
            {
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return answer;
        }

        // SHOW CUSTOMER
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_Customer";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMER BY ID
        public (DataTable, string) ShowById()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerById";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure
                };

                SqlParameter customerId = new SqlParameter
                {
                    ParameterName = "@idCustomer", SqlDbType = SqlDbType.Int, Value = CustomerId
                };
                command.Parameters.Add(customerId);

                SqlParameter showMode = new SqlParameter
                {
                    ParameterName = "@mode", SqlDbType = SqlDbType.Int, Value = ModeToShowById
                };
                command.Parameters.Add(showMode);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // ACTIVE OR INACTIVE CUSTOMERS
        public (DataTable, string) ShowCustomerByState(string state)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByState";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter customerState = new SqlParameter { ParameterName = "@state", SqlDbType = SqlDbType.VarChar, Size = 8, Value = state };
                command.Parameters.Add(customerState);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMERS BY IDENTIFICATION CARD
        public (DataTable, string) ShowCustomerByIdentificationCard()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByIdentificationCard";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter identificationCard = new SqlParameter { ParameterName = "@identificationCard", SqlDbType = SqlDbType.VarChar, Size = 16, 
                    Value = IdentificationCard };
                command.Parameters.Add(identificationCard);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMERS BY PHONE NUMBER
        public (DataTable, string) ShowCustomerByPhoneNumber()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByPhoneNumber";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter phoneNumber = new SqlParameter { ParameterName = "@phoneNumber", SqlDbType = SqlDbType.VarChar, Size = 20,
                    Value = PhoneNumber };
                command.Parameters.Add(phoneNumber);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMERS BY NAME OR LASTNAME
        public (DataTable, string) ShowCustomersByNameOrLastname(string nameOrLastname)
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByNameOrLastname";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter customerNameOrLastname = new SqlParameter { ParameterName = "@nameOrlastname", SqlDbType = SqlDbType.VarChar, Size = 60,
                    Value = nameOrLastname };
                command.Parameters.Add(customerNameOrLastname);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMERS BY CITY
        public (DataTable, string) ShowCustomerByCity()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByCity";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter city = new SqlParameter { ParameterName = "@city", SqlDbType = SqlDbType.VarChar, Size = 50,
                    Value = City };
                command.Parameters.Add(city);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        // SHOW CUSTOMERS BY BILLING ADDRESS
        public (DataTable, string) ShowCustomerByBillingAddress()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_CustomerByBillingAddress";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand { Connection = connection, CommandText = sqlQuery, CommandType = CommandType.StoredProcedure };

                SqlParameter billingAddress = new SqlParameter { ParameterName = "@billingAddress", SqlDbType = SqlDbType.VarChar, Size = 150,
                    Value = BillingAddress };
                command.Parameters.Add(billingAddress);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                dataTable = null;
                answer = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }
    }
}
