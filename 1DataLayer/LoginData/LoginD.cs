﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace _1DataLayer
{
    public class LoginD : ISIUD
    {
        public int EmployeeId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public LoginD()
        {
        }

        // TO INSERT AND UPDATE
        public LoginD(int employeeId, string username, string password)
        {
            EmployeeId = employeeId;
            Username = username;
            Password = password;
        }

        public string Insert()
        {
            throw new NotImplementedException();
        }

        public string Update()
        {
            throw new NotImplementedException();
        }
        
        // TO SHOW
        public (DataTable, string) Show()
        {
            string answer = null;
            DataTable dataTable = new DataTable();
            string sqlQuery = "S_ApplicationLogin";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = sqlQuery, Connection = connection, CommandType = CommandType.StoredProcedure
                };

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {
                answer = ex.Message;
                dataTable = null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (dataTable, answer);
        }

        public (DataTable, string) ShowById()
        {
            throw new NotImplementedException();
        }

        public (int, string) AppLogin_Verifier()
        {
            string answer = null;
            string sqlQuery = "AppLogin_Verifier";
            SqlConnection connection = new SqlConnection(Connection.connectionString);

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = sqlQuery, Connection = connection, CommandType = CommandType.StoredProcedure
                };

                SqlParameter user = new SqlParameter
                {
                    ParameterName = "@username", SqlDbType = SqlDbType.VarChar, Size = 50, Value = Username
                };
                command.Parameters.Add(user);

                SqlParameter userPassword = new SqlParameter
                {
                    ParameterName = "@userPassword", SqlDbType = SqlDbType.VarChar, Size = 160, Value = Password
                };
                command.Parameters.Add(userPassword);

                EmployeeId = Convert.ToInt32(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                answer = ex.Message;
                EmployeeId = 0;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }

            return (EmployeeId, answer);
        }
    }
}
